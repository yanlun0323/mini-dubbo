package dubbo.learn.cluster.loadbalance;

import dubbo.learn.common.ProviderHostAndPort;

import java.util.List;

public interface LoadBalancePolicy {

    /**
     * 负载均衡策略，从指定的服务提供者集合中选择一个
     * @param list
     * @return
     */
    ProviderHostAndPort selectOne(List<ProviderHostAndPort> list);
}
