package dubbo.learn.core.component.export;

import lombok.Data;

@Data
public class ExportService{

    /**
     * 远端服务的全名
     */
    private String exportServiceFullName;


}
