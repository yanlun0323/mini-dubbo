package dubbo.learn.core.component.protocol;

import lombok.Data;

/**
 * 服务提供者使用
 * <dubbo:protocol></dubbo:protocol> 对应的领域对象
 */
@Data
public class ServiceProtocol {
    /**
     * 协议名
     */
    private String protocolName;

    /**
     * 可选，当有多个网卡时，最好手动指定这个
     */
    private String host;

    /**
     * 端口
     */
    private Integer port;
}
